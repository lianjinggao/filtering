#include <math.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>

using namespace cv;
using namespace std;


//
// global variables
//

int refreshFlag=1; // indicate whether the display needs to be refreshed



// YOU MAY ADD MORE FUNCTIONS IF YOU NEED

void convertGrayscale(Mat &src,Mat &dst)
{

}

void singleColor(Mat &src, Mat &dst)
{

}

void xConvFilter(Mat &src, Mat &dst)
{

}

void yConvFilter(Mat &src, Mat &dst)
{

}

void gaussianSmoothing(Mat &img,float sigma, int amountSmoothing)
{

}


void computeGradientVectors(Mat &img, Mat &grad, Mat &gradMag)
{

}


void selectGradientsForDisplay(Mat &grad, Mat &gradMag, float prcnt)
{

}


void drawGradientVectors(Mat &grad, Mat &img)
{

}

void rotateImage(Mat &src, Mat &dst, int theta)
{

}


void trackbarHandlerAM(int pos)
{
  refreshFlag=1;
}

void trackbarHandlerPP(int pos)
{
  refreshFlag=1;
}

void trackbarHandlerRA(int pos)
{
  refreshFlag=1;
}

void help(void)
{
  cout << endl;
  cout << "Usage: ass1 [<image-file>]" << endl;
  cout << endl;
  cout << "Key summary:" << endl;
  cout << "---------------------------------" << endl;
  cout << endl;
  cout << "<ESC>: quit" << endl;
  cout << "'i' - reload the original image (i.e. cancel any previous processing)" << endl;
  cout << "'w' - save the current (possibly processed) image into the file 'out.jpg'" << endl;
  cout << "'g' - convert the image to grayscale using the openCV conversion function." << endl;
  cout << "'G' - convert the image to grayscale using your implementation of conversion function." << endl;
  cout << "'c' - cycle through the color channels of the image showing a different channel every time the key is pressed." << endl;
  cout << "'s' - convert the image to grayscale and smooth it using the openCV function. Use a track bar to control the amount of smoothing." << endl;
  cout << "'S' - convert the image to grayscale and smooth it using your function which should perform convolution with a suitable filter. Use a track bar to control the amount of smoothing." << endl;
  cout << "'x' - convert the image to grayscale and perform convolution with an x derivative filter. Normalize the obtained values to the range [0,255]." << endl;
  cout << "'y' - convert the image to grayscale and perform convolution with a y derivative filter. Normalize the obtained values to the range [0,255]." << endl;
  cout << "'m' - show the magnitude of the gradient normalized to the range [0,255]. The gradient is computed based on the x and y derivatives of the image." << endl;
  cout << "'p' - convert the image to grayscale and plot the gradient vectors of the image every N pixels and let the plotted gradient vectors have a length of K. Use a track bar to control N. Plot the vectors as short line segments of length K." << endl;
  cout << "'r' - convert the image to grayscale and rotate it using an angle of Q degrees. Use a track bar to control the rotation angle. The rotation of the image should be performed using an inverse map so there are no holes in it." << endl;
  cout << "'h' - Display a short description of the program, its command line arguments, and the keys it supports." << endl;
  cout << endl;
}


int main(int argc, char *argv[])
{
  int displayMode=1;   // the current display mode
  int amountSmoothing;
  int pixelPercentage; // the percentage of pixels in which the
                       // image gradient should be displayed
  int rotAngle;
  float sigma=1.0;     // the standard deviation of the Gaussian filter

  Mat cimg;  // the original color image
  Mat gimg;  // the grayscale image
  Mat scimg; // single channel image: show R,G,B once a time
  Mat ximg;  // result of x convolution
  Mat yimg;  // result of y convolution

  Mat grad;      // the gradient map
  Mat gradMag;   // the gradient magnitude

  Mat rimg;  // rotated image

  Mat tmpImg;    // temporary image
  Mat outImg;
  int i,j,k,key;


  // capture an image from a camera or read it from a file
  if(argc<2){
    CvCapture* capture = cvCaptureFromCAM(0);
    if(cvGrabFrame(capture)) cimg=cvarrToMat(cvRetrieveFrame(capture));
  }
  else cimg=imread(argv[1], CV_LOAD_IMAGE_COLOR);

  cout << "OpenCV version: " << CV_VERSION <<  endl;



  // check the read image
  if(cimg.empty()){
    cout << "Could not read/grab image" << endl;
    exit(0);
  }

  // create a window with three trackbars
  cvNamedWindow("win1", CV_WINDOW_AUTOSIZE);
  cvMoveWindow("win1", 100, 100);

  amountSmoothing=50;
  cvCreateTrackbar("amount of smoothing", "win1", &amountSmoothing ,100, trackbarHandlerAM);

  pixelPercentage=10;
  cvCreateTrackbar("N pixels", "win1", &pixelPercentage ,20, trackbarHandlerPP);

  rotAngle=0;
  cvCreateTrackbar("rotation angle Q", "win1", &rotAngle ,360, trackbarHandlerRA);

  // create the image pyramid


  // enter the keyboard event loop
  while(1){

    key=cvWaitKey(10); // wait 10 ms for a key
    if(key==27) break;

    switch(key){
    case 'i':
      displayMode=1;
      refreshFlag=1;
      break;
    case 'g':
      displayMode=2;
      refreshFlag=1;
      break;

// write your own code to switch
    case 'G':
    case 'c':
    case 's':
    case 'S':
    case 'x':
    case 'y':
    case 'm':
    case 'p':
    case 'r':

    case 'w':
      if(!imwrite("out.jpg",outImg)) cout << "file save error" << endl;
      break;
    case 'h':
      help();
      break;
    }

    // update the display as necessary
    if(refreshFlag){
      refreshFlag=0;
      switch(displayMode){

      case 1:
        imshow("win1",cimg);    //show original image
        outImg=cimg.clone();    //clone current image to output image
        break;

      case 2:
        convertGrayscale(cimg,gimg);  // call your function for convertion
        imshow("win1",gimg);    //show grayscale image
        outImg=cimg.clone();    //clone current image to output image

      case 3:
      case 4:
      // add more cases to switch
        break;
      }
    }

  }

  // release the images
  cimg.release();
  return 0;
}
