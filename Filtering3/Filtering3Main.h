/***************************************************************
 * Name:      Filtering3Main.h
 * Purpose:   Defines Application Frame
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-13
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#ifndef FILTERING3MAIN_H
#define FILTERING3MAIN_H

//(*Headers(Filtering3Frame)
#include <wx/menu.h>
#include <wx/slider.h>
#include <wx/frame.h>
#include <wx/statusbr.h>
//*)
#include "FilteringAlgController.hpp"
#include "wxImagePanel.hpp"

class Filtering3Frame: public wxFrame
{
    public:

        Filtering3Frame(wxWindow* parent,wxWindowID id = -1);
        virtual ~Filtering3Frame();

    private:
        friend class FilteringAlgController;
        wxImagePanel * drawPane;
        FilteringAlgController * thread;

        //(*Handlers(Filtering3Frame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnChoose(wxCommandEvent& event);
        void OnCam(wxCommandEvent& event);
        void OnSlider1CmdScroll(wxScrollEvent& event);
        //*)

        //(*Identifiers(Filtering3Frame)
        static const long ID_SLIDER2;
        static const long ID_SLIDER1;
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(Filtering3Frame)
        wxSlider* Slider1;
        wxSlider* Slider2;
        wxStatusBar* StatusBar1;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // FILTERING3MAIN_H
