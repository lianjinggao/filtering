/***************************************************************
 * Name:      Filtering3App.h
 * Purpose:   Defines Application Class
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-13
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#ifndef FILTERING3APP_H
#define FILTERING3APP_H

#include <wx/app.h>

class Filtering3App : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // FILTERING3APP_H
