/***************************************************************
 * Name:      Filtering3Main.cpp
 * Purpose:   Code for Application Frame
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-13
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#include "Filtering3Main.h"
#include <wx/msgdlg.h>

//(*InternalHeaders(Filtering3Frame)
#include <wx/intl.h>
#include <wx/string.h>
//*)

BEGIN_EVENT_TABLE(Filtering3Frame,wxFrame)

    //(*EventTable(Filtering3Frame)
    //*)
END_EVENT_TABLE()


//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(Filtering3Frame)
const long Filtering3Frame::ID_SLIDER2 = wxNewId();
const long Filtering3Frame::ID_SLIDER1 = wxNewId();
const long Filtering3Frame::idMenuQuit = wxNewId();
const long Filtering3Frame::idMenuAbout = wxNewId();
const long Filtering3Frame::ID_STATUSBAR1 = wxNewId();
//*)

Filtering3Frame::Filtering3Frame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(Filtering3Frame)
    wxMenuItem* MenuItem2;
    wxMenuItem* MenuItem1;
    wxMenu* Menu1;
    wxMenuBar* MenuBar1;
    wxMenu* Menu2;

    Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));
    SetClientSize(wxSize(985,567));
    Slider2 = new wxSlider(this, ID_SLIDER2, 0, 0, 100, wxPoint(0,512), wxSize(900,50), 0, wxDefaultValidator, _T("ID_SLIDER2"));
    Slider1 = new wxSlider(this, ID_SLIDER1, 0, 0, 100, wxPoint(0,450), wxSize(900,50), 0, wxDefaultValidator, _T("ID_SLIDER1"));
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem1 = new wxMenuItem(Menu1, idMenuQuit, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);
    StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
    int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar1);

    Connect(idMenuQuit,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&Filtering3Frame::OnQuit);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&Filtering3Frame::OnAbout);
    //*)
}

void Filtering3Frame::OnQuit(wxCommandEvent& event)
{
    thread->setState(-1);
    Sleep(2000);
    Close();
}

void Filtering3Frame::OnCam(wxCommandEvent& event)
{
    //thread->Pause();
    thread->setState(2);
    //thread->Resume();
}

void Filtering3Frame::OnChoose(wxCommandEvent& event)
{
    thread->Pause();
    thread->setState(1);
    wxString caption = wxT("Choose a file");
    wxString wildcard = wxT("JPG_files_(*.jpg)|*.jpg|BMP_files_(*.bmp)|*.bmp|GIF_files_(*.gif)|*.gif");
    wxString defaultDir = wxT ("c:\\");

    wxString defaultFilename = wxEmptyString ;
    wxFileDialog dialog (NULL, caption, defaultDir , defaultFilename,
                         wildcard , 0);
    if (dialog.ShowModal () == wxID_OK)
    {
        wxString path = dialog.GetPath();
        thread->path = path.ToStdString();
        //int filterIndex = dialog.GetFilterIndex();
    }

    thread->Resume();

}

void Filtering3Frame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void Filtering3Frame::OnSlider1CmdScroll(wxScrollEvent& event)
{
}
