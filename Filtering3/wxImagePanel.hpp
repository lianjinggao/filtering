#ifndef WXIMAGEPANEL_HPP_INCLUDED
#define WXIMAGEPANEL_HPP_INCLUDED


#include <wx/wx.h>
#include <wx/sizer.h>

class wxImagePanel : public wxPanel
{
private:
    wxBitmap resized;
    int w, h;
public:

    wxImage image;
    wxImagePanel(wxFrame* parent, wxString file, wxBitmapType format);

    void paintEvent(wxPaintEvent & evt);
    void paintNow();
    void fresh();
    void OnSize(wxSizeEvent& event);
    void render(wxDC& dc);

    // some useful events
    /*
     void mouseMoved(wxMouseEvent& event);
     void mouseDown(wxMouseEvent& event);
     void mouseWheelMoved(wxMouseEvent& event);
     void mouseReleased(wxMouseEvent& event);
     void rightClick(wxMouseEvent& event);
     void mouseLeftWindow(wxMouseEvent& event);
     void keyPressed(wxKeyEvent& event);
     void keyReleased(wxKeyEvent& event);
     */

    DECLARE_EVENT_TABLE()
};


#endif // WXIMAGEPANEL_HPP_INCLUDED
