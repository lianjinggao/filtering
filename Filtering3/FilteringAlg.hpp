#ifndef FILTERINGALG_HPP_INCLUDED
#define FILTERINGALG_HPP_INCLUDED

//(*OpenCV
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace cv;
using namespace std;
//*)OpenCV


class FilteringAlg{



public:
    FilteringAlg();

    Mat *Origin;
    Mat *Origin_gray;
    Mat *Result;

    void argRead();

    void show_origin();//i
    void opencv_grayscale();//g
    void grayscale();//G
    void cycleChannel();//c

    void opencv_grayscale_smooth();//s
    void grayscale_smooth();//S
    void opencv_grayscale_x();
    void opencv_grayscale_y();

    int save();//w
    int reload();//i



};


#endif // FILTERINGALG_HPP_INCLUDED
