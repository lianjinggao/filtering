/***************************************************************
 * Name:      Filtering3App.cpp
 * Purpose:   Code for Application Class
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-13
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#include "Filtering3App.h"

//(*AppHeaders
#include "Filtering3Main.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(Filtering3App);

bool Filtering3App::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	Filtering3Frame* Frame = new Filtering3Frame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
