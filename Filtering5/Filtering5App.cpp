/***************************************************************
 * Name:      Filtering5App.cpp
 * Purpose:   Code for Application Class
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-17
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "Filtering5App.h"
#include "Filtering5Main.h"

IMPLEMENT_APP(Filtering5App);

bool Filtering5App::OnInit()
{
    wxInitAllImageHandlers();
    Filtering5Frame* frame = new Filtering5Frame(0L);
    frame->SetIcon(wxICON(aaaa)); // To Set App Icon
    frame->Show();
    SetTopWindow(frame);
    return true;
}
