#ifndef WXIMAGEPANEL_HPP_INCLUDED
#define WXIMAGEPANEL_HPP_INCLUDED


#include <wx/wx.h>
#include <wx/sizer.h>

#define idSilder 2000
#define idText 2001

class wxImagePanel : public wxPanel
{
private:
    wxBitmap resized;
    int w, h;
public:
    int algState;
    int pState;
    wxImage image;
    wxImagePanel(wxFrame* parent, wxString file, wxBitmapType format);
    wxTextCtrl * textc;
    wxSlider * slider;
    int getArg();


    void paintEvent(wxPaintEvent & evt);
    void paintNow();
    void OnSize(wxSizeEvent& event);
    void render(wxDC& dc);
    void keyReleased(wxKeyEvent& event);

    // some useful events
    /*
     void mouseMoved(wxMouseEvent& event);
     void mouseDown(wxMouseEvent& event);
     void mouseWheelMoved(wxMouseEvent& event);
     void mouseReleased(wxMouseEvent& event);
     void rightClick(wxMouseEvent& event);
     void mouseLeftWindow(wxMouseEvent& event);
     void keyPressed(wxKeyEvent& event);
     void keyReleased(wxKeyEvent& event);
     */

    DECLARE_EVENT_TABLE()
};


#endif // WXIMAGEPANEL_HPP_INCLUDED
