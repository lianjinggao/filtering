#include "wxImagePanel.hpp"

BEGIN_EVENT_TABLE(wxImagePanel, wxPanel)
// some useful events
/*
 EVT_MOTION(wxImagePanel::mouseMoved)
 EVT_LEFT_DOWN(wxImagePanel::mouseDown)
 EVT_LEFT_UP(wxImagePanel::mouseReleased)
 EVT_RIGHT_DOWN(wxImagePanel::rightClick)
 EVT_LEAVE_WINDOW(wxImagePanel::mouseLeftWindow)
 EVT_KEY_DOWN(wxImagePanel::keyPressed)
 EVT_KEY_UP(wxImagePanel::keyReleased)
 EVT_MOUSEWHEEL(wxImagePanel::mouseWheelMoved)
 */

// catch paint events
EVT_PAINT(wxImagePanel::paintEvent)
//Size event
EVT_SIZE(wxImagePanel::OnSize)
EVT_KEY_UP(wxImagePanel::keyReleased)
END_EVENT_TABLE()


// some useful events
/*
 void wxImagePanel::mouseMoved(wxMouseEvent& event) {}
 void wxImagePanel::mouseDown(wxMouseEvent& event) {}
 void wxImagePanel::mouseWheelMoved(wxMouseEvent& event) {}
 void wxImagePanel::mouseReleased(wxMouseEvent& event) {}
 void wxImagePanel::rightClick(wxMouseEvent& event) {}
 void wxImagePanel::mouseLeftWindow(wxMouseEvent& event) {}
 void wxImagePanel::keyPressed(wxKeyEvent& event) {}
 void wxImagePanel::keyReleased(wxKeyEvent& event) {}
 */

wxImagePanel::wxImagePanel(wxFrame* parent, wxString file, wxBitmapType format) :
wxPanel(parent)
{
    // load the file... ideally add a check to see if loading was successful
    //image.LoadFile(_("image1.jpg"), format);
    image.LoadFile(file, format);
    w = -1;
    h = -1;
    slider = new wxSlider(this, idSilder , 0,0,100,wxDefaultPosition,wxDefaultSize,wxSL_HORIZONTAL, wxDefaultValidator,_("slider"));
    wxPoint newPoint(0,30);
    textc = new wxTextCtrl(this, idText, _("No Performance yet"), newPoint,wxDefaultSize, 0 , wxDefaultValidator,_("text"));
    this->SetFocusIgnoringChildren();
    this->SetFocus();
}

/*
 * Called by the system of by wxWidgets when the panel needs
 * to be redrawn. You can also trigger this call by
 * calling Refresh()/Update().
 */

void wxImagePanel::paintEvent(wxPaintEvent & evt)
{
    // depending on your system you may need to look at double-buffered dcs
    wxPaintDC dc(this);
    render(dc);
    this->SetFocusIgnoringChildren();
    this->SetFocus();
}

/*
 * Alternatively, you can use a clientDC to paint on the panel
 * at any time. Using this generally does not free you from
 * catching paint events, since it is possible that e.g. the window
 * manager throws away your drawing when the window comes to the
 * background, and expects you will redraw it when the window comes
 * back (by sending a paint event).
 */
void wxImagePanel::paintNow()
{
    // depending on your system you may need to look at double-buffered dcs
    wxClientDC dc(this);
    //wxBufferedDC dc(this);
    //wxPaintDC dc(this);
    render(dc);
    int neww, newh;
    dc.GetSize( &neww, &newh );
    resized = wxBitmap( image.Scale( neww, newh /*, wxIMAGE_QUALITY_HIGH*/ ) );
    w = neww;
    h = newh;
    dc.DrawBitmap( resized, 0, 0, false );
    Refresh();
}


/*
 * Here we do the actual rendering. I put it in a separate
 * method so that it can work no matter what type of DC
 * (e.g. wxPaintDC or wxClientDC) is used.
 */
void wxImagePanel::render(wxDC&  dc)
{
    int neww, newh;
    dc.GetSize( &neww, &newh );

    if( neww != w || newh != h )
    {
        resized = wxBitmap( image.Scale( neww, newh /*, wxIMAGE_QUALITY_HIGH*/ ) );

        w = neww;
        h = newh;
        dc.DrawBitmap( resized, 0, 0, false );
    }else{
        dc.DrawBitmap( resized, 0, 0, false );
    }
}

/*
 * Here we call refresh to tell the panel to draw itself again.
 * So when the user resizes the image panel the image should be resized too.
 */
void wxImagePanel::OnSize(wxSizeEvent& event){
    Refresh();
    //skip the event.
    event.Skip();
}


int wxImagePanel::getArg(){
    return this->slider->GetValue();
}

void wxImagePanel::keyReleased(wxKeyEvent& event){
    bool x = GetKeyState(WXK_SHIFT);
    if(event.GetKeyCode()== 73 ){ //73 == i
        algState = 0;
    }

    else if(event.GetKeyCode()==71&&x ){// 71 ==G
        algState = 2;
    }
    else if(event.GetKeyCode()==66 ){// 71 ==b
        algState = 1;
    }
    else if(event.GetKeyCode()==67 ){// 71 ==c
        if(pState == 3){
            algState = 4;
            pState = 4;
        }
        else if(pState == 4){
            algState = 5;
            pState = 5;
        }
        else if(pState == 5){
            algState = 3;
            pState =3;
        }
        else{
            algState = 3;
            pState =3;
        }
    }
    else if(event.GetKeyCode()==65 ){//83 ==a

        algState = 7;
    }
    else if(event.GetKeyCode()==83 ){//83 ==s

        algState = 6;
    }
    else if(event.GetKeyCode()==88 ){//83 ==x

        algState = 8;
    }
    else if(event.GetKeyCode()==89 ){//83 ==y

        algState = 9;
    }
    else if(event.GetKeyCode()==77 ){//77 ==m
        algState = 10;
    }
    else if(event.GetKeyCode()==82 ){//77 ==r
        algState = 11;
    }
    else if(event.GetKeyCode()==87 ){//77 ==r
        algState = 12;
    }
    else if(event.GetKeyCode()==80 ){//77 ==r
        algState = 13;
    }

}
