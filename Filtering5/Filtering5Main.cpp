/***************************************************************
 * Name:      Filtering5Main.cpp
 * Purpose:   Code for Application Frame
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-17
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "Filtering5Main.h"

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__WXMAC__)
        wxbuild << _T("-Mac");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}


Filtering5Frame::Filtering5Frame(wxFrame *frame)
    : GUIFrame(frame)
{
#if wxUSE_STATUSBAR
    statusBar->SetStatusText(_("Hello Code::Blocks user!"), 0);
    statusBar->SetStatusText(wxbuildinfo(short_f), 1);
#endif
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    wxImagePanel * pane = new wxImagePanel( this, wxT("image1.jpg"), wxBITMAP_TYPE_JPEG);
    sizer->Add(pane, 1, wxEXPAND);
    this->SetSizer(sizer);

    thread = new FilteringAlgController(pane);
    if(thread->Create()!=wxTHREAD_NO_ERROR){
        wxLogError(wxT("Can't create thread!"));
    }
    thread->Run();
}

Filtering5Frame::~Filtering5Frame()
{
    //thread->Kill();
    //delete(thread);
    //delete(drawPane);
}

void Filtering5Frame::OnCam(wxCommandEvent &event)
{
    thread->setState(2);
}
void Filtering5Frame::OnFile(wxCommandEvent &event)
{
    thread->Pause();
    thread->setState(1);
    wxString caption = wxT("Choose a file");
    wxString wildcard = wxT("JPG_files_(*.jpg)|*.jpg|BMP_files_(*.bmp)|*.bmp|GIF_files_(*.gif)|*.gif");

    char cwd[1024];
    getcwd(cwd, sizeof(cwd));
    std::string str(cwd);
    wxString defaultDir = _(str);


    wxString defaultFilename = wxEmptyString ;
    wxFileDialog dialog (NULL, caption, defaultDir , defaultFilename,
                         wildcard , 0);
    if (dialog.ShowModal () == wxID_OK)
    {
        wxString path = dialog.GetPath();
        thread->path = path.ToStdString();
        //int filterIndex = dialog.GetFilterIndex();
    }
    else{
        thread->setState(0);
    }
    thread->Resume();
}
void Filtering5Frame::OnClose(wxCloseEvent &event)
{
    thread->setState(-1);
    Sleep(3000);
    Destroy();
}

void Filtering5Frame::OnQuit(wxCommandEvent &event)
{
    thread->setState(-1);
    //Sleep(5000);
    Destroy();
}

void Filtering5Frame::OnAbout(wxCommandEvent &event)
{
    wxString msg2 = _T("'i' - reload the original image (i.e. cancel any previous processing)\r");
    wxString msg3 = _T("'w' - save the current (possibly processed) image into the file 'out.jpg'\r");
    wxString msg4 = _T("'g' - convert the image to grayscale using the openCV conversion function.");
    wxString msg5 = _T("'G' - convert the image to grayscale using your implementation of conversion function.\r");
    wxString msg6 = _T("'c' - cycle through the color channels of the image showing a different channel every time the key is pressed.\r");
    wxString msg7 = _T("'s' - convert the image to grayscale and smooth it using the openCV function. Use a track bar to control the amount of smoothing.\r");
    wxString msg8 = _T("'S' - convert the image to grayscale and smooth it using your function which should perform convolution with a suitable filter. Use a track bar to control the amount of smoothing.\r");
    wxString msg9 = _T("'x' - convert the image to grayscale and perform convolution with an x derivative filter. Normalize the obtained values to the range [0,255].\r");
    wxString msg10= _T("'y' - convert the image to grayscale and perform convolution with a y derivative filter. Normalize the obtained values to the range [0,255].\r");
    wxString msg11= _T("'m' - show the magnitude of the gradient normalized to the range [0,255]. The gradient is computed based on the x and y derivatives of the image.\r");
    wxString msg12= _T("'p' - convert the image to grayscale and plot the gradient vectors of the image every N pixels and let the plotted gradient vectors have a length of K. Use a track bar to control N. Plot the vectors as short line segments of length K.\r");
    wxString msg13= _T("'r' - convert the image to grayscale and rotate it using an angle of Q degrees. Use a track bar to control the rotation angle. The rotation of the image should be performed using an inverse map so there are no holes in it.\r");
    wxString msg14= _T("'h' - Display a short description of the program, its command line arguments, and the keys it supports.");

    wxString msg = msg2+msg3+msg4+msg5+msg6+msg7+msg8+msg9+msg10+msg11+msg12+msg13+msg14;
    wxMessageBox(msg, _("This is a description!"));
}


