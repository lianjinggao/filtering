#ifndef FILTERINGALGCONTROLLER_HPP_INCLUDED
#define FILTERINGALGCONTROLLER_HPP_INCLUDED

#include <wx/wx.h>
#include <time.h>
#include <sstream>
#include "FilteringAlg.hpp"
#include <iostream>
#include "opencv2/opencv.hpp"
#include "wxImagePanel.hpp"

using namespace cv;
using namespace std;

class FilteringAlgController : public wxThread{
public:
    int arg;
    string path;
    int state;// 0 for camera state 1 for image state
    int algState;

    FilteringAlgController(wxImagePanel * pane);
    wxImagePanel *  drawPane;

    virtual ExitCode Entry();

    //(*
    void *  mat2wxImage(Mat &frame, wxImage  &image);
    Mat convertType(const Mat& srcImg, int toType, double alpha, double beta);
    void setArg(int);
    void setState(int);
    //*)
private:
    FilteringAlg *alg;
    Mat * Origin;
    Mat * Result;
    void argDetect();
    void runAlg();


};

#endif // FILTERINGALGCONTROLLER_HPP_INCLUDED
