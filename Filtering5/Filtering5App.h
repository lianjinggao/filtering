/***************************************************************
 * Name:      Filtering5App.h
 * Purpose:   Defines Application Class
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-17
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#ifndef FILTERING5APP_H
#define FILTERING5APP_H

#include <wx/app.h>
#include "wxImagePanel.hpp"
#include "FilteringAlgController.hpp"
class Filtering5App : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // FILTERING5APP_H
