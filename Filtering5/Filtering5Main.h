/***************************************************************
 * Name:      Filtering5Main.h
 * Purpose:   Defines Application Frame
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-17
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#ifndef FILTERING5MAIN_H
#define FILTERING5MAIN_H



#include "Filtering5App.h"
#include "FilteringAlgController.hpp"

#include "GUIFrame.h"

class Filtering5Frame: public GUIFrame
{
    public:
        Filtering5Frame(wxFrame *frame);
        ~Filtering5Frame();
        wxImagePanel * drawPane;
    private:
        FilteringAlgController * thread;
        virtual void OnClose(wxCloseEvent& event);
        virtual void OnQuit(wxCommandEvent& event);
        virtual void OnAbout(wxCommandEvent& event);
        virtual void OnCam(wxCommandEvent& event);
        virtual void OnFile(wxCommandEvent& event);
};

#endif // FILTERING5MAIN_H
