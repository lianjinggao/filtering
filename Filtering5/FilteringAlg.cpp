#include "FilteringAlg.hpp"

FilteringAlg::FilteringAlg(){
    this->Origin_gray = new Mat();
}


void FilteringAlg::mygrayscale(Mat * src, Mat * des ){
    delete(des);
    des = new Mat(src->size(),CV_8UC1);
    for (int j=0; j<des->rows; j++) {
        for (int i=0; i<des->cols; i++) {
            des->at<uchar>(j,i) =
            0.299 * src->at<Vec3b>(j,i)[0]+ 0.587 *src->at<Vec3b>(j,i)[1]
            +0.114* src->at<Vec3b>(j,i)[2];
        }
    }
}



void FilteringAlg::opencv_grayscale(){
    cvtColor(*Origin, *Result, CV_RGB2GRAY);
    Origin_gray = Result;
}

void FilteringAlg::show_origin(){
    *(this->Result) = Origin->clone();

}

void FilteringAlg::grayscale(){
    Result =  new Mat(Origin->size(),CV_8UC1);
    for (int j=0; j<Result->rows; j++) {
        for (int i=0; i<Result->cols; i++) {
            Result->at<uchar>(j,i) =
            0.299 * Origin->at<Vec3b>(j,i)[0]+ 0.587 *Origin->at<Vec3b>(j,i)[1]
            +0.114* Origin->at<Vec3b>(j,i)[2];
        }
    }

}//G

void FilteringAlg::cycleChannel0(){
    delete(Result);
    Result =  new Mat(Origin->size(),CV_8UC3);
    for (int j=0; j<Result->rows; j++) {
        for (int i=0; i<Result->cols; i++) {
            Result->at<Vec3b>(j,i)[0] =Origin->at<Vec3b>(j,i)[0];
            Result->at<Vec3b>(j,i)[1] =0;
            Result->at<Vec3b>(j,i)[2] =0;;

        }
    }
}

void FilteringAlg::cycleChannel1(){
    delete(Result);
    Result =  new Mat(Origin->size(),CV_8UC3);
    for (int j=0; j<Result->rows; j++) {
        for (int i=0; i<Result->cols; i++) {
            Result->at<Vec3b>(j,i)[0] =0;
            Result->at<Vec3b>(j,i)[1] =Origin->at<Vec3b>(j,i)[1];
            Result->at<Vec3b>(j,i)[2] =0;

        }
    }
}

void FilteringAlg::cycleChannel2(){
    delete(Result);
    Result =  new Mat(Origin->size(),CV_8UC3);
    for (int j=0; j<Result->rows; j++) {
        for (int i=0; i<Result->cols; i++) {
            Result->at<Vec3b>(j,i)[0] =0;
            Result->at<Vec3b>(j,i)[1] =0;
            Result->at<Vec3b>(j,i)[2] =Origin->at<Vec3b>(j,i)[2];

        }
    }
}

void FilteringAlg::opencv_grayscale_smooth(){
    int amountx;
    if(arg == 0){
        amountx = 3;
    }
    else{
        int refv = min(Origin->cols,Origin->rows);
        amountx = arg*floor(refv/100);
        if(amountx%2==0){
            amountx++;
        }

    }
    cvtColor(*Origin, *Origin_gray, CV_RGB2GRAY);
    blur(*Origin_gray , *Result, Size(amountx,amountx));

};

void FilteringAlg::grayscale_smooth(){
    int amountx;
    if(arg == 0){
        amountx = 30;
    }
    else{
        int refv = min(Origin->cols,Origin->rows);
        amountx = arg*floor(refv/100)/2;
    }
    this->mygrayscale(Origin, Origin_gray);
    Point anchor = Point( -1, -1 );
    double delta = 0;
    int ddepth = -1;
    int ind = 30;
    int kernel_size = 3 + 2*( amountx );
    Mat kernel = Mat::ones( kernel_size, kernel_size, CV_32F )/(float)(kernel_size*kernel_size);
    filter2D(*Origin_gray, *Result, ddepth , kernel, anchor, delta, BORDER_DEFAULT );

}

void FilteringAlg::opencv_grayscale_x(){
    this->mygrayscale(Origin, Origin_gray);
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;
    Mat grad_x;
    Sobel( *Origin_gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_x, *Result );
}

void FilteringAlg::opencv_grayscale_y(){
    this->mygrayscale(Origin, Origin_gray);
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;
    Mat grad_x;
    Sobel( *Origin_gray, grad_x, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_x, *Result );
}

void FilteringAlg::opencv_gradient_vector(){
    int amountx;
    if(arg == 0){
        amountx = 3;
    }
    else{
        amountx = arg/4;
        if(amountx%2==0){
            amountx++;
        }
    }

    Mat * temp = new Mat();
    this->mygrayscale(Origin, temp);
    GaussianBlur(*temp,*Origin_gray,Size(5,5),1.0);
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    Sobel( *Origin_gray, grad_x, ddepth, 1, 0, amountx, scale, delta, BORDER_DEFAULT );
    Sobel( *Origin_gray, grad_y, ddepth, 0, 1, amountx, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x );
    convertScaleAbs( grad_y, abs_grad_y );
    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, *Result);
}

void FilteringAlg::opencv_grayscale_xy(){



    this->mygrayscale(Origin, Origin_gray);
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    Sobel( *Origin_gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
    Sobel( *Origin_gray, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x );
    convertScaleAbs( grad_y, abs_grad_y );
    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, *Result);
}


void FilteringAlg::opencv_rotate(){
    int amountx;
    if(arg == 0){
        amountx = 0;
    }
    else{
        amountx = arg*floor(360/100);
    }


    int len = std::max(Origin->cols, Origin->rows);
    Point2f pt(len/2,len/2);
    Mat r = getRotationMatrix2D(pt,amountx,1.0);
    warpAffine(*Origin, *Result, r, Size(len, len));

}

void FilteringAlg::setArg(int in){
    this->arg = in;
}

void FilteringAlg::save(){
    imwrite("output.jpg", *Result );
}
