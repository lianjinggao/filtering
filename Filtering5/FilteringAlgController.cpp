#include "FilteringAlgController.hpp"

FilteringAlgController::FilteringAlgController(wxImagePanel * pane):wxThread(wxTHREAD_JOINABLE){
    this->state = 0;
    this->drawPane = pane;
    this->Origin = new Mat();
    this->Result = new Mat();
    alg = new FilteringAlg();
    alg->Origin = this->Origin;
    alg->Result = this->Result;
    algState = 0;

}


wxThread::ExitCode FilteringAlgController::Entry(){
    clock_t tstart,tend;


    EntryPoint2:
    while(!TestDestroy())
    {
        switch(this->state){
        case 1:{
            for(;;){

                *Origin = imread(path,CV_LOAD_IMAGE_COLOR);
                tstart = clock();
                runAlg();
                tend =clock()-tstart;
                ostringstream temp;
                temp<<tend;
                this->drawPane->textc->SetValue(_(temp.str()));
                mat2wxImage( *Result, drawPane->image);
                drawPane->paintNow();
                if(state !=1 ){
                    goto EntryPoint2;
                }
            }
            //this->Pause();
        }break;
        case 2:{
            VideoCapture cap(0);
            for(;;)
            {
                cap >> *Origin;
                tstart = clock();
                runAlg();
                tend =clock()-tstart;
                ostringstream temp;
                temp<<tend;
                this->drawPane->textc->SetValue(_(temp.str()));
                mat2wxImage( *Result, drawPane->image);
                drawPane->paintNow();
                if(state !=2 ){
                     goto EntryPoint2;
                }
            }

        }break;
        case -1:{
            goto EntryPoint1;
        }
        };

    }
    EntryPoint1:
    return this->Wait();
    //return 0;
}

void FilteringAlgController::setArg(int in){
    this->arg = in;
}

void FilteringAlgController::setState(int in){
    this->state = in;
}

void * FilteringAlgController::mat2wxImage( Mat &frame, wxImage  &image){

        // data dimension
    int w = frame.cols, h = frame.rows;
    int size = w*h*3*sizeof(unsigned char);

    // allocate memory for internal wxImage data
    unsigned char * wxData = (unsigned char*) malloc(size);

    // the matrix stores BGR image for conversion
    Mat cvRGBImg = Mat(h, w, CV_8UC3, wxData);

    switch (frame.channels())
    {
    case 1: // 1-channel case: expand and copy
    {
      // convert type if source is not an integer matrix
      if (frame.depth() != CV_8U)
      {
        cvtColor(convertType(frame, CV_8U, 255,0), cvRGBImg, CV_GRAY2RGB);
      }
      else
      {
        cvtColor(frame, cvRGBImg, CV_GRAY2RGB);
      }
    } break;

    case 3: // 3-channel case: swap R&B channels
    {
      int mapping[] = {0,2,1,1,2,0}; // CV(BGR) to WX(RGB)
      mixChannels(&frame, 1, &cvRGBImg, 1, mapping, 3);
    } break;

    default:
    {
      wxLogError(wxT("Cv2WxImage : input image (#channel=%d) should be either 1- or 3-channel"), frame.channels());
    }
  }

  image.Destroy(); // free existing data if there's any
  image = wxImage(w, h, wxData);

}

Mat FilteringAlgController::convertType(const Mat& srcImg, int toType, double alpha, double beta)
{
  Mat dstImg;
  srcImg.convertTo(dstImg, toType, alpha, beta);
  return dstImg;
}

void FilteringAlgController::runAlg(){
    algState = drawPane->algState;
    alg->setArg(drawPane->getArg());
    if(algState == 0){
        alg->show_origin();
    }
    else if(algState == 1){
        alg->opencv_grayscale();
    }
    else if(algState == 2){
        alg->grayscale();
    }
    else if(algState == 3){
        alg->cycleChannel0();
    }
    else if(algState == 4){
        alg->cycleChannel1();
    }
    else if(algState == 5){
        alg->cycleChannel2();
    }
    else if(algState == 6){
        alg->opencv_grayscale_smooth();
    }
    else if(algState == 7){
        alg->grayscale_smooth();
    }
    else if(algState == 8){
        alg->opencv_grayscale_x();
    }
    else if(algState == 9){
        alg->opencv_grayscale_y();
    }
    else if(algState == 10){
        alg->opencv_grayscale_xy();
    }
    else if(algState == 11){
        alg->opencv_rotate();
    }
    else if(algState == 12){
        alg->save();
    }
    else if(algState == 13){
        alg->opencv_gradient_vector();
    }
}
