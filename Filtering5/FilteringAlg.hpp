#ifndef FILTERINGALG_HPP_INCLUDED
#define FILTERINGALG_HPP_INCLUDED

//(*OpenCV
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace cv;
using namespace std;
//*)OpenCV


class FilteringAlg{



public:
    FilteringAlg();

    int arg;
    void setArg(int);

    Mat *Origin;
    Mat *Origin_gray;
    Mat *Result;

    void argRead();

    void show_origin();//i
    void opencv_grayscale();//g
    void grayscale();//G
    void cycleChannel0();//c
    void cycleChannel1();//c
    void cycleChannel2();//c

    void opencv_grayscale_smooth();//s
    void grayscale_smooth();//S
    void opencv_grayscale_x();
    void opencv_grayscale_y();
    void opencv_grayscale_xy();
    void opencv_gradient_vector();

    void opencv_rotate();

    void save();//w
    int reload();//i

    //(*Tools
    void mygrayscale(Mat * src, Mat * des);


    //Tools*)
};


#endif // FILTERINGALG_HPP_INCLUDED
