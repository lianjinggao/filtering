/***************************************************************
 * Name:      FilteringMain.h
 * Purpose:   Defines Application Frame
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-08
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#ifndef FILTERINGMAIN_H
#define FILTERINGMAIN_H

//(*Headers(FilteringDialog)
#include <wx/sizer.h>
#include <wx/statline.h>
#include <wx/slider.h>
#include <wx/panel.h>
#include <wx/button.h>
#include <wx/dialog.h>
//*)

#include "wxImagePanel.hpp"

class FilteringDialog: public wxDialog
{
    public:

        FilteringDialog(wxWindow* parent,wxWindowID id = -1);
        virtual ~FilteringDialog();

    private:

        //(*Handlers(FilteringDialog)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnChoose(wxCommandEvent& event);
        void OnCam(wxCommandEvent& event);
        void OnInit(wxInitDialogEvent& event);
        void ModeInit();
        void OnTextCtrl1Text(wxCommandEvent& event);
        //*)

        //(*Identifiers(FilteringDialog)
        static const long ID_BUTTON5;
        static const long ID_STATICLINE2;
        static const long ID_BUTTON6;
        static const long ID_BUTTON7;
        static const long ID_BUTTON8;
        static const long ID_PANEL1;
        static const long ID_SLIDER1;
        //*)

        //(*Declarations(FilteringDialog)
        wxSlider* Slider1;
        wxPanel* Panel1;
        wxBoxSizer* BoxSizer2;
        wxStaticLine* StaticLine2;
        wxButton* Button6;
        wxButton* Button5;
        wxButton* Button7;
        wxBoxSizer* BoxSizer1;
        wxBoxSizer* BoxSizer3;
        wxButton* Button8;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // FILTERINGMAIN_H
