/***************************************************************
 * Name:      FilteringApp.cpp
 * Purpose:   Code for Application Class
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-08
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#include "FilteringApp.h"

//(*AppHeaders
#include "FilteringMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(FilteringApp);

bool FilteringApp::OnInit()
{

    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	FilteringDialog Dlg(0);
    	SetTopWindow(&Dlg);
    	Dlg.ShowModal();
    	wxsOK = false;
    }
    //*)
    return wxsOK;
}
