#ifndef FILTERINGALG_HPP_INCLUDED
#define FILTERINGALG_HPP_INCLUDED

//(*OpenCV
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace cv;
using namespace std;
//*)OpenCV


class FilteringAlg{

private:
    Mat *Origin;
    Mat *Origin_gray;
    Mat *Result;

public:

    void argRead();

    void show_origin();//i
    void opencv_grayscale();//g
    void grayscale();//G
    void opencv_grayscale_smooth(int in);//s
    void grayscale_smooth(int in);//S
    void opencv_grayscale_x();
    void opencv_grayscale_y();

    int save();//w
    int reload();//i



};


#endif // FILTERINGALG_HPP_INCLUDED
