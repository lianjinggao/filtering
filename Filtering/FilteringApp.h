/***************************************************************
 * Name:      FilteringApp.h
 * Purpose:   Defines Application Class
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-08
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#ifndef FILTERINGAPP_H
#define FILTERINGAPP_H

#include <wx/app.h>

class FilteringApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // FILTERINGAPP_H
