/***************************************************************
 * Name:      FilteringMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    LianjingGao (lgao6@hawk.iit.edu)
 * Created:   2014-02-08
 * Copyright: LianjingGao (xunshuairu@gmail.com)
 * License:
 **************************************************************/

#include "FilteringMain.h"
#include "FilteringAlgController.hpp"

#include <wx/msgdlg.h>
#include <wx/filedlg.h>
//(*OpenCV
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace cv;
using namespace std;
//*)OpenCV



//(*InternalHeaders(FilteringDialog)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(FilteringDialog)
const long FilteringDialog::ID_BUTTON5 = wxNewId();
const long FilteringDialog::ID_STATICLINE2 = wxNewId();
const long FilteringDialog::ID_BUTTON6 = wxNewId();
const long FilteringDialog::ID_BUTTON7 = wxNewId();
const long FilteringDialog::ID_BUTTON8 = wxNewId();
const long FilteringDialog::ID_PANEL1 = wxNewId();
const long FilteringDialog::ID_SLIDER1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(FilteringDialog,wxDialog)
    //(*EventTable(FilteringDialog)
    //*)
END_EVENT_TABLE()

FilteringDialog::FilteringDialog(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(FilteringDialog)
    Create(parent, wxID_ANY, _("wxWidgets app"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("wxID_ANY"));
    BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    BoxSizer3 = new wxBoxSizer(wxVERTICAL);
    Button5 = new wxButton(this, ID_BUTTON5, _("Description"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON5"));
    BoxSizer3->Add(Button5, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 4);
    StaticLine2 = new wxStaticLine(this, ID_STATICLINE2, wxDefaultPosition, wxSize(10,-1), wxLI_HORIZONTAL, _T("ID_STATICLINE2"));
    BoxSizer3->Add(StaticLine2, 0, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 4);
    Button6 = new wxButton(this, ID_BUTTON6, _("Open"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON6"));
    BoxSizer3->Add(Button6, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Button7 = new wxButton(this, ID_BUTTON7, _("Camera"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON7"));
    BoxSizer3->Add(Button7, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Button8 = new wxButton(this, ID_BUTTON8, _("Quit"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON8"));
    BoxSizer3->Add(Button8, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 4);
    BoxSizer1->Add(BoxSizer3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer2 = new wxBoxSizer(wxVERTICAL);
    Panel1 = new wxPanel(this, ID_PANEL1, wxDefaultPosition, wxSize(429,298), wxTAB_TRAVERSAL, _T("ID_PANEL1"));
    BoxSizer2->Add(Panel1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Slider1 = new wxSlider(this, ID_SLIDER1, 0, 0, 100, wxDefaultPosition, wxSize(507,10), 0, wxDefaultValidator, _T("ID_SLIDER1"));
    BoxSizer2->Add(Slider1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    BoxSizer1->Add(BoxSizer2, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 4);
    SetSizer(BoxSizer1);
    BoxSizer1->Fit(this);
    BoxSizer1->SetSizeHints(this);

    Connect(ID_BUTTON5,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&FilteringDialog::OnAbout);
    Connect(ID_BUTTON8,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&FilteringDialog::OnQuit);
    Connect(wxID_ANY,wxEVT_INIT_DIALOG,(wxObjectEventFunction)&FilteringDialog::OnInit);
    //*)


    //*(Controller thread start

    FilteringAlgController *thread = new FilteringAlgController();
    if(thread->Create(wxTHREAD_JOINABLE)!=wxTHREAD_NO_ERROR)
    {
        wxLogError(wxT("Cann't create thread!"));
    }
    thread->Run();
    //*)
}

FilteringDialog::~FilteringDialog()
{
    //(*Destroy(FilteringDialog)
    //*)
}

void FilteringDialog::OnQuit(wxCommandEvent& event)
{
    Close();
}

void FilteringDialog::OnChoose(wxCommandEvent& event)
{
    wxString caption = wxT("Choose a file");

    wxString wildcard = wxT("BMP_files_(*.bmp)|*.bmp|GIF_files_(*.gif)|*.gif");
    wxString defaultDir = wxT ("c:\\");

    wxString defaultFilename = wxEmptyString ;
    wxFileDialog dialog (NULL, caption, defaultDir , defaultFilename,
                         wildcard , 0);
    if (dialog.ShowModal () == wxID_OK)
    {
        wxString path = dialog.GetPath();
        //int filterIndex = dialog.GetFilterIndex();
    }

}

void FilteringDialog::OnCam(wxCommandEvent& event)
{
    VideoCapture cap(0);
    if(!cap.isOpened()) return;

    Mat frame;
    Mat out_frame;
    namedWindow("OpenCV");
    for(;;)
    {
        cap >> frame;

        blur( frame, out_frame, Size( 10, 10 ) );

        imshow("OpenCV", out_frame);
        if(waitKey(30) >= 0){
            destroyWindow("OpenCV");
            return;
        }

    }
}


void FilteringDialog::OnAbout(wxCommandEvent& event)
{

    wxString msg2 = _T("'i' - reload the original image (i.e. cancel any previous processing)\r");
    wxString msg3 = _T("'w' - save the current (possibly processed) image into the file 'out.jpg'\r");
    wxString msg4 = _T("'g' - convert the image to grayscale using the openCV conversion function.");
    wxString msg5 = _T("'G' - convert the image to grayscale using your implementation of conversion function.\r");
    wxString msg6 = _T("'c' - cycle through the color channels of the image showing a different channel every time the key is pressed.\r");
    wxString msg7 = _T("'s' - convert the image to grayscale and smooth it using the openCV function. Use a track bar to control the amount of smoothing.\r");
    wxString msg8 = _T("'S' - convert the image to grayscale and smooth it using your function which should perform convolution with a suitable filter. Use a track bar to control the amount of smoothing.\r");
    wxString msg9 = _T("'x' - convert the image to grayscale and perform convolution with an x derivative filter. Normalize the obtained values to the range [0,255].\r");
    wxString msg10= _T("'y' - convert the image to grayscale and perform convolution with a y derivative filter. Normalize the obtained values to the range [0,255].\r");
    wxString msg11= _T("'m' - show the magnitude of the gradient normalized to the range [0,255]. The gradient is computed based on the x and y derivatives of the image.\r");
    wxString msg12= _T("'p' - convert the image to grayscale and plot the gradient vectors of the image every N pixels and let the plotted gradient vectors have a length of K. Use a track bar to control N. Plot the vectors as short line segments of length K.\r");
    wxString msg13= _T("'r' - convert the image to grayscale and rotate it using an angle of Q degrees. Use a track bar to control the rotation angle. The rotation of the image should be performed using an inverse map so there are no holes in it.\r");
    wxString msg14= _T("'h' - Display a short description of the program, its command line arguments, and the keys it supports.");

    wxString msg = msg2+msg3+msg4+msg5+msg6+msg7+msg8+msg9+msg10+msg11+msg12+msg13+msg14;
    wxMessageBox(msg, _T("Description"));
}

void FilteringDialog::OnInit(wxInitDialogEvent& event)
{
}

void FilteringDialog::OnTextCtrl1Text(wxCommandEvent& event)
{
}
