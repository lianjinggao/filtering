#ifndef FILTERINGALGCONTROLLER_HPP_INCLUDED
#define FILTERINGALGCONTROLLER_HPP_INCLUDED

#include <wx/wx.h>
#include "FilteringAlg.hpp"
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace cv;
using namespace std;

class FilteringAlgController : public wxThread{
public:
    int arg;
    int state;

    virtual void * Entry();
private:

    FilteringAlg *alg;
    Mat * Origin;
    Mat * Result;
    void argDetect();
    void setArg(int);
    void setState(int);



};

#endif // FILTERINGALGCONTROLLER_HPP_INCLUDED
